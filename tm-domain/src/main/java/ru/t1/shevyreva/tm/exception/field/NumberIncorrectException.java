package ru.t1.shevyreva.tm.exception.field;

public final class NumberIncorrectException extends AbsrtactFieldException {

    public NumberIncorrectException() {
        super("Error! Number is incorrect!");
    }

    public NumberIncorrectException(final String value, final Throwable cause) {
        super("Error! This value " + value + " is incorrect!");
    }
}
