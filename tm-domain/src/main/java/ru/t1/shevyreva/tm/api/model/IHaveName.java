package ru.t1.shevyreva.tm.api.model;

import org.jetbrains.annotations.NotNull;

public interface IHaveName {

    @NotNull
    String getName();

    void setName(@NotNull String name);

}
