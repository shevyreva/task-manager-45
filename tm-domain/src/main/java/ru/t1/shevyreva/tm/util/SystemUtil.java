package ru.t1.shevyreva.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.lang.management.ManagementFactory;

public interface SystemUtil {

    static long getPID() {
        @Nullable final String processname = ManagementFactory.getRuntimeMXBean().getName();
        if (processname != null && processname.length() > 0)
            try {
                return Long.parseLong(processname.split("@")[0]);
            } catch (@NotNull final Exception e) {
                return 0;
            }
        return 0;
    }

}
