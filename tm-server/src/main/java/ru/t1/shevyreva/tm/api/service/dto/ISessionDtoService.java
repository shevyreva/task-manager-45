package ru.t1.shevyreva.tm.api.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.model.SessionDTO;


public interface ISessionDtoService {

    @NotNull
    @SneakyThrows
    SessionDTO add(@Nullable final String userId, @Nullable final SessionDTO session);

    @NotNull
    @SneakyThrows
    SessionDTO add(@Nullable final SessionDTO session);

    @NotNull
    @SneakyThrows
    SessionDTO removeOne(@Nullable final SessionDTO session);

    @SneakyThrows
    boolean existsById(@NotNull final String id);

}
