package ru.t1.shevyreva.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.api.repository.model.IProjectRepository;
import ru.t1.shevyreva.tm.api.service.IConnectionService;
import ru.t1.shevyreva.tm.api.service.model.IProjectService;
import ru.t1.shevyreva.tm.enumerated.ProjectSort;
import ru.t1.shevyreva.tm.enumerated.Status;
import ru.t1.shevyreva.tm.exception.entity.ModelNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.StatusNotFoundException;
import ru.t1.shevyreva.tm.exception.entity.TaskNotFoundException;
import ru.t1.shevyreva.tm.exception.field.*;
import ru.t1.shevyreva.tm.model.Project;
import ru.t1.shevyreva.tm.repository.model.ProjectRepository;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;


public final class ProjectService implements IProjectService {

    private final IConnectionService connectionService;

    public ProjectService(@NotNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }


    @NotNull
    @Override
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();


        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.addForUser(userId, project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();

        @NotNull final Project project = new Project();
        project.setName(name);

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.addForUser(userId, project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name, final String description
    ) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize(userId) < index) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final Project project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize(userId) < index) throw new IndexEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final Project project = findOneByIndex(userId, index);
        project.setStatus(status);
        if (project == null) throw new ProjectNotFoundException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final Project project = findOneById(userId, id);
        project.setStatus(status);
        if (project == null) throw new ProjectNotFoundException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId, @Nullable final ProjectSort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            if (sort == null) return projectRepository.findAllForUser(userId);
            if (sort.getDisplayName() != null) return projectRepository.findAllForUser(userId, sort.getComparator());
            else return null;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @Nullable final Project project = projectRepository.findOneByIdForUser(userId, id);
            if (project == null) throw new TaskNotFoundException();
            return project;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        if (getSize(userId) < index) throw new IndexEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            @Nullable final Project project = projectRepository.findOneByIndexForUser(userId, index);
            if (project == null) throw new TaskNotFoundException();
            return project;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public int getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            return projectRepository.getSizeForUser(userId);
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project removeOne(@Nullable final String userId, @Nullable final Project project) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (project == null) throw new ModelNotFoundException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.removeOneByIdForUser(userId, project.getId());
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Project removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            Project project = projectRepository.findOneByIdForUser(userId, id);
            entityManager.getTransaction().begin();
            projectRepository.removeOneByIdForUser(userId, id);
            entityManager.getTransaction().commit();
            return project;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            entityManager.getTransaction().begin();
            projectRepository.clearForUser(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            return projectRepository.findAll();
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @SneakyThrows
    public Collection<Project> add(@NotNull final Collection<Project> models) {
        if (models == null) throw new ProjectNotFoundException();

        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            for (@NotNull Project project : models) {
                projectRepository.add(project);
            }
            entityManager.getTransaction().commit();
            return models;
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @SneakyThrows
    public void removeAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            projectRepository.clear();
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }

    }

    @NotNull
    @SneakyThrows
    public Collection<Project> set(@NotNull final Collection<Project> models) {
        @Nullable final Collection<Project> entities;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            removeAll();
            entityManager.getTransaction().begin();
            entities = add(models);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return entities;
    }

    public boolean existsById(@NotNull final String user_id, @NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
            return (projectRepository.findOneByIdForUser(user_id, id) != null);
        } finally {
            entityManager.close();
        }
    }


}
